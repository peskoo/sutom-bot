from confz import BaseConfig, FileSource


class Config(BaseConfig):
    token: str
    chat_id: int

    CONFIG_SOURCES = FileSource(file="./config/config.yaml")
