import logging
from typing import Optional

import yaml

logger = logging.getLogger(__name__)


def load_data():
    with open("./database/database.yaml", "r") as read_file:
        data = yaml.safe_load(read_file)
    return data


def get_day(day: str) -> Optional[str]:
    data = load_data()
    day = data.get(day, None)

    if day:
        logger.info(f"get_day: {day}")
        return day
    else:
        logger.error("This day is not in database")


def already_played_today(name: str, day: str) -> bool:
    daily_list = get_day(day)

    if daily_list is None:
        return False

    for participation in daily_list:
        if participation.get("name") == name:
            return True

    return False
