import logging
import yaml
from typing import Dict, Union

from .get import already_played_today, load_data

logger = logging.getLogger(__name__)


def write_data(new_data: Dict[str, Union[str, int]], day: str):
    data = load_data()
    today_list = data.get(day)
    logger.info("today_list: {0}".format(today_list))

    if today_list:
        today_list.append(new_data)
    else:
        data.update({day: [new_data]})

    logger.info("Database updated: {0}".format(data))
    if data:
        with open("./database/database.yaml", "w") as file:
            yaml.safe_dump(data, file)


def add(name: str, shots: int, day: str):
    if already_played_today(name, day):
        logger.info(f"{name} has already played today.")
    else:
        logger.info(f"Adding new participation for {name}.")
        new_entry = {"name": name, "shots": shots}

        write_data(new_entry, day)

        logger.info(f"New participation for {name} added.")
