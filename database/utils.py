import calendar
import datetime
import itertools
import logging
from typing import Union, NewType

import yaml

from .get import load_data

logger = logging.getLogger(__name__)
Data = NewType("Data", dict[str, list[dict[str, Union[str, int]]]])
SummarizedData = NewType("SummarizedData", dict[str, dict[str, int]])
OrderedData = NewType("OrderedData", dict[str, dict[str, int]])
FinalData = NewType("FinalData", dict[str, dict[str, int]])


def format_day(date: datetime.datetime) -> str:
    """
    return format day.month.year
    """
    return f"{date.day}.{date.month}.{date.year}"


def summarize_data(data: Data) -> SummarizedData:
    """Format data to summarized by name all activities in the month."""
    # {
    #   "name X": {"total_shots": int, "total_days": int},
    #   "name Y": {"total_shots": int, "total_days": int},
    #   "name Z": {"total_shots": int, "total_days": int},
    # }
    logger.info("Data: {}".format(data))
    result = {}

    # The list of players in daily_list, match with just one day.
    # We iterate on each days.
    for daily_list in data.values():
        # daily_list: [{'name': 'Pesko', 'shots': 4}, {'name': 'Claire', 'shots': 7}]

        for player in daily_list:
            # player: {'name': 'Pesko', 'shots': 4}

            name = player.get("name")

            # If player already in result
            if name in list(result.keys()):
                # Increment total_shots
                result[name]["total_shots"] += player.get("shots")
                # Increment total_days
                result[name]["total_days"] += 1

            # If player not in result
            else:
                # Create entry with player's shots and total_days init to 1.

                p_dict = {name: {"total_shots": player["shots"], "total_days": 1}}
                result.update(p_dict)

    logger.info("Result computing for the month: {}".format(result))
    return result


def ordered_data(data: FinalData) -> OrderedData:
    # sorted() will transform in tuple
    # In our lambda 'value' == ('Name', {'total_shots': 1, 'total_days': 1})
    # So value[1] for the second element of our tuple
    # Then value[1]["total_shots"] to order our dict by total_shots
    return dict(sorted(data.items(), key=lambda value: value[1]["total_shots"]))


def handle_days_with_no_participation(data: SummarizedData) -> FinalData:
    # We must compare the number of days for this month to total_days
    # The difference permit to add a number to handle the days without participation
    # 1 day without participation ---> add 6 to total_shots.
    # Why 6 ?
    # Maximum score in sutom is 5, after you loose. So 6.

    # OrderedData:
    # {
    #   'X': {'total_shots': 1, 'total_days': 1},
    #   'Y': {'total_shots': 5, 'total_days': 29},
    #   'T': {'total_shots': 7, 'total_days': 14},
    #   'Z': {'total_shots': 11, 'total_days': 2}
    # }

    current_month: int = datetime.datetime.now().month
    current_year: int = datetime.datetime.now().year
    days_in_this_month: int = calendar.monthrange(current_year, current_month)[1]

    for stats in data.values():
        total_days = stats["total_days"]

        # If total_days is strictely inferior to days in the month.
        # Actual month contains 31 days.
        # total_days of participation for X is 22.
        # 31 - 22 = 9.
        # X missed 9 days.
        # We add the penalty 9 * 6 = 54 to total_shots.
        if total_days < days_in_this_month:
            penalty = (days_in_this_month - total_days) * 6
            stats["total_shots"] += penalty

    return data


def get_podium(data: SummarizedData):
    podium = dict(itertools.islice(data.items(), 3))
    logger.info("Ordered podium for the month: {}".format(podium))
    return podium


def calculate_top_3() -> dict[str, int]:
    data = load_data()
    logger.info("Calculating top 3...")

    data_of_the_month: SummarizedData = summarize_data(data)
    result: FinalData = handle_days_with_no_participation(data_of_the_month)
    ordered_dict: OrderedData = ordered_data(result)
    podium = get_podium(ordered_dict)

    return podium


def erase_database():
    with open("./database/database.yaml", "w") as file:
        yaml.safe_dump({}, file)

    logger.info("Database reinitialised.")
