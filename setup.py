from setuptools import find_packages, setup

INSTALL_REQUIRES = [
    "confz==2.0.1",
    "PyYAML==6.0.1",
    "python-telegram-bot[job-queue]==20.7",
]

VERSION = "0.0.0a"


setup(
    name="sutom-bot",
    version=VERSION,
    packages=find_packages(),
    url="https://lasalledutemps.fr",
    author="Pesko",
    author_email="pesko@lasalledutemps.fr",
    description="Who is the best sutomers of the month.",
    install_requires=INSTALL_REQUIRES,
    extras_require={
        "test": [
            "flake8>=4.0.1",
            "mypy>=0.971",
            "pre-commit>=2.20.0",
            "pytest>=7.1.2",
            "pytest-cov>=3.0.0",
        ]
    },
)
