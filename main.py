import datetime
import logging
import re
from typing import Final, Optional

from telegram import Update
from telegram.ext import (
    Application,
    CommandHandler,
    MessageHandler,
    filters,
    ContextTypes,
)

from config.config import Config
from database.add import add
from database.utils import calculate_top_3, erase_database, format_day

logging.basicConfig(
    encoding="utf-8",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)

BOT_USERNAME: Final = "@sutom_bot"
TELEGRAM_TOKEN: Final = Config().token
SUTOM_GROUP_CHAT_ID: Final = Config().chat_id


async def start_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text("Hello ! J'adore Sutom.")


def add_shots_to_database(name: str, shots: int, date: str):
    add(name, shots, date)


def handle_sutom_shots(message: str, date: datetime.datetime, name: str) -> None:
    text: str = message.split("en")[1]
    shots: int = int(re.search(r"\d+", text).group())

    logger.info(f"Participant: {name}, shots: {shots}, date: {date}")
    add_shots_to_database(name, shots, format_day(date))


# Responses
def handle_response() -> str:
    return "Je ne suis pas là pour papoter avec vous. Je posterais le Top 3 des participants à la fin du mois. Bisous."


async def handle_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message_type: str = update.message.chat.type
    text: str = update.message.text
    response: Optional[str] = None

    logger.info(
        f"Chat ID: {update.message.chat.id}, Room type: {message_type}, Message: {text}"
    )

    if message_type == "group":
        if BOT_USERNAME in text:
            # new_text: str = text.replace(BOT_USERNAME, "").strip()
            response: str = handle_response()
        elif "J'ai trouvé le mot" in text:
            # Retrieve the sutom shots for every one.
            handle_sutom_shots(
                text, update.message.date, update.message.from_user.first_name
            )
        else:
            return
    else:
        response: str = handle_response()

    if response:
        logger.info(f"Bot response: {response}")
        await update.message.reply_text(response)


async def error(update: Update, context: ContextTypes.DEFAULT_TYPE):
    logger.error(f"Update: {update}, caused error -> {context.error}")


async def callback_monthly(context: ContextTypes.DEFAULT_TYPE):
    podium = list(calculate_top_3())

    erase_database()

    len_podium = len(podium)
    if len_podium == 3:
        format_message = f"🥉 {podium[2]}   🥈 {podium[1]}    🏆 {podium[0]}"
    elif len_podium == 2:
        format_message = f"🥈 {podium[1]}    🏆 {podium[0]}"
    elif len_podium == 1:
        format_message = f"🏆 {podium[0]}"
    else:
        format_message = "Aucun participant ce mois ci."

    await context.bot.send_message(
        chat_id=context.job.chat_id, text="🥁 Le classement du mois:"
    )
    await context.bot.send_message(chat_id=context.job.chat_id, text=format_message)


def main():
    logger.info("Hello Sutom... building...")

    # Build
    app = Application.builder().token(TELEGRAM_TOKEN).build()
    job_queue = app.job_queue

    # Testing periodic with minute period.
    # job_minute = job_queue.run_repeating(
    #     callback_monthly, interval=60, first=10, chat_id=SUTOM_GROUP_CHAT_ID
    # )

    # Periodic job run each last day of the month at 23h59.
    job_monthly = job_queue.run_monthly(
        callback_monthly,
        when=datetime.time(23, 59),
        day=-1,
        chat_id=SUTOM_GROUP_CHAT_ID,
    )

    # Commands
    app.add_handler(CommandHandler("start", start_command))

    # Messages
    app.add_handler(MessageHandler(filters.TEXT, handle_message))

    # Errors
    app.add_error_handler(error)

    # Poll interval in seconds
    logger.info("Polling...")
    app.run_polling(poll_interval=3)


if __name__ == "__main__":
    main()
